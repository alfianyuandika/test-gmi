const express = require("express");

// Import controller
const userController = require("../controllers/userController");

// Import auth (middleware)
const auth = require("../middlewares/auth");

// Make router
const router = express.Router();

router.get("/", userController.getAll);
router.get("/find", userController.getOne);
router.patch("/update/:id", userController.update);
router.delete("/delete", userController.delete);

// router.route("/").post(userController.signUp);


module.exports = router;
