const express = require("express"); // Import express
const passport = require("passport"); // Import passport


const authValidator = require("../middlewares/validators/authValidator");

const authController = require("../controllers/authController");

const auth = require("../middlewares/auth");

const router = express.Router();


router.post(
  "/signup",
  authValidator.signup,
  auth.signup,
  authController.register
);

router.post(
  "/signin",
  authValidator.signin,
  auth.signin,
  authController.getToken
);

router.get(
  "/verify",
  auth.verify,
  (req, res) => {
    res.redirect("https://www.google.com/");
  },
  authController.getToken
);

module.exports = router;
