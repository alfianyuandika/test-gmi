require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

const fs = require("fs");
const path = require("path");
const morgan = require("morgan");


// Express
const express = require("express");

const app = express();

if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
} else {
  let accessLogStream = fs.createWriteStream(
    path.join(__dirname, "access.log"),
    {
      flags: "a",
    }
  );

  app.use(morgan("combined", { stream: accessLogStream }));
}


// Import routes
const authRoutes = require("./routes/authRoutes");
const userRoutes = require("./routes/userRoutes");


// Make express app

// Body-parser to read req.body
app.use(express.json()); // Enable req.body JSON type
app.use(
  express.urlencoded({
    extended: true,
  })
); // Support urlencode body

// Make routes
app.use("/auth", authRoutes);
app.use("/user", userRoutes)


const PORT = 3000 || process.env.PORT;
app.listen(PORT, () => console.log(`server running on:${PORT}`));

module.exports = app


