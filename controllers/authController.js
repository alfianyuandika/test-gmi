const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const fs = require("fs");
let mailVerify = fs.readFileSync(__dirname + "/WashMe2.html", {
  encoding: "utf-8",
});

class AuthController {
  async getToken(req, res) {
    try {
      const body = {
        id: req.user._id,
        email: req.user.email,
      };

      const token = jwt.sign(body, process.env.JWT_SECRET, {
        expiresIn: "30d",
      });

      const name = req.body.name;
      const email = req.user.email;
      const status = req.user.status;

      if (req.user.status != "Active") {
        // console.log(req.user.status);
        console.log("halo kamu");

        return res.status(401).send({
          message: "Pending Account. Please Verify Your Email!",
          // url: `http://localhost:3000/user/verify?token=${token}`,
        });
      }

      return res.status(200).json({
        message: "Success",
        name,
        token,
        email,
        status,
      });
    } catch (e) {
      console.log("test lagi", e);
      return res.status(500).json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  }

  async register(req, res) {
    try {
      const body = {
        user: {
          id: req.user.id,
          email: req.user.email,
          status: req.user.status,
        },
      };

      const token = jwt.sign(body, process.env.JWT_SECRET, {
        expiresIn: "60d",
      });
      // Nodemailer 57-86
      const transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user: process.env.EMAIL,
          pass: process.env.PASSWORD,
        },

        authentication: "plain",
        address: "smtp.gmail.com",
        port: 587,
      });
      // console.log("token", token);
      // console.log("status", req.user.status)
      mailVerify = mailVerify.replace(
        "Verify_endpoint",
        process.env.VERIFY_URL + "?token=" + token
      );

      // console.log(req.user.email);
      let mailOptions = {
        from: "alfiangmi1001@gmail.com",
        to: `${req.user.email}`,
        subject: "Email Registration for user",
        html: mailVerify,
      };

      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
        } else {
          //console.log("Email sent: " + info.response);
        }
      });

      const id = body.user.id;
      const email = req.user.email;
      const status = req.user.status;
      return res.status(200).json({
        message: "Please check your email for verification",
        id,
        email,
        token,
        status,
      });
    } catch (e) {
      // console.log(e);
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new AuthController();
